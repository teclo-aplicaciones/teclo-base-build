# Aplicación base (BaseApp)

`Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.`

## Servidor de desarrollo

Usar `ng serve` Para correr la aplicación en modo desarrollo. Por defecto la aplicación corre en `http://localhost:4200/`.
La aplicación detecta automáticamente los cambios y recarga

## Generación de recursos

Usar `ng generate component component-name` o `ng g c component-name` Para generar nuevos componentes.
También se puede usar `ng generate directive|pipe|service|class|guard|interface|enum|module` o `ng g d|p|s|class|g|i|enum|m`.

## Ej.
## Generar un componente que genere componente html, ts, y una carpeta nueva
ng g c pages/about -is --spec=false
## Generar un componente que genere componente html, ts, sin una carpeta nueva
ng g c pages/about -is --spec=false --flat

## Construir la aplicación

Usar `ng build` para construir el proyecto compilado.
Los archivos compilados serán generados en el directorio `dist/`.
Usar la bandera `--prod` para generar un compilado de producción.

## Ejecutar documentación de compodoc

Usa `npm install --save @compodoc/compodoc` para instalar compodoc.
Usar `npm run compodoc` para ejecutar compodoc.
Se generará un directorio llamado `documentation`.
Para ver la web de documentación del proyecto usar `compodoc -r 8080 -s` donde `8080` es el puerto en el que desee abrir la web.

## Ejecutar pruebas unitarias

Usar `ng test` para ejecutar las pruebas unitarias vía [Karma](https://karma-runner.github.io).

## Ejecutando pruebas end-to-end (extremo a extremo)

Usar `ng e2e` para ejecutar pruebas end-to-end via [Protractor](http://www.protractortest.org/).

## Ayuda adicional

Para obtener más ayuda sobre comandos de angular-cli y sus funciones usar `ng help`. Ejemplo: `ng g --help`. O dirijirse al sitio [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

by: `César Gómez`
