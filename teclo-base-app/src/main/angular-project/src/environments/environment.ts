// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// const uriWs = '/assets/data/jsons';
const uriWs = 'http://ext.teclo.mx/teclocdmxgc_v230r2_prod_api';
// const uriWs = 'http://200.57.32.64/sspcdmxsai_v620r1_pro_api';
// const uriWs = 'http://172.18.44.152/teclocdmxgc_api_v230r1';

const protocol = `${location.protocol}//`;
const host = location.host;
// const url = `${protocol}${host}${uriWs}`;
const url = uriWs;
const absUrl = location.href;
const uriCli = `${absUrl.split('/')[3]}`;
const skipUris = [ '/anything' ];

export const Assets = `${protocol}${host}/assets`;

export const Environment = {
  production: false,
  urlWs: url,
  absUrl,
  uriCli,
  skipUris
};
