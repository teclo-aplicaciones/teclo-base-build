// const uriWs = '/assets/data/jsons';
const uriWs = 'http://ext.teclo.mx/teclocdmxgc_v230r2_prod_api_STAGINS';

const protocol = `${location.protocol}//`;
const host = location.host;
// const url = `${protocol}${host}${uriWs}`;
const url = uriWs;
const absUrl = location.href;
const uriCli = `/${absUrl.split('/')[3]}`;
const skipUris = [ '/anything' ];

export const Assets = `${protocol}${host}/assets`;

export const Environment = {
  production: false,
  urlWs: url,
  absUrl,
  uriCli,
  skipUris
};
