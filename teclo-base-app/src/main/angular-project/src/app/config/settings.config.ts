import { Injectable } from '@angular/core';
import { StorageNameVO, AppSettingsVO, Settings, InactivityVO } from '@models/app/application.model';
import { Menu } from '@models/app/menu.model';
import { UserVO } from '@models/app/user.model';


@Injectable()
export class AppSettings {

    public storageName: StorageNameVO;
    public configurationApp: AppSettingsVO;
    public settings: Settings;
    public isLoadConfig: boolean;
    public user: UserVO;
    public isToken: boolean;
    public menu: Menu[];
    public isLoadMenu: boolean;
    public version = '1.0.0';
    public inactivity = new InactivityVO(
        false, false, false, 0, 0, '', '', null, null
    );

}
