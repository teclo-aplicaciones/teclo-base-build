export class CambioContrasenia {

  constructor(
        public currentPassword: string,
        public newPassword: string,
        public repeatPassword: string
    ) { }
}
