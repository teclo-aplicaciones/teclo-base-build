export class StorageNameVO {
    constructor(
        public ambient: string,
        public base: string,
        public ambientbase: string,
        public token: string,
        public statusinactivity: string,
        public statusrequest: string,
        public config: string,
        public configtmp: string,
        public wishlist: string
    ) { }
}

export class ApplicationVO {

    constructor(
        public idAplicacion: number,
        public codigo: string,
        public nombre: string,
        public descripcion: string,
        public ambiente: string
    ) { }

}

export class ResolutionVO {

    constructor(
        public idResolucion: number,
        public cdResolucion: string,
        public nbResolucion: string,
        public nuPixelesBase: string,
        public txResolucion: string,
        public stActivo: number
    ) { }

}

export class ThemeVO {

    constructor(
        public idTema: number,
        public nbTema: string,
        public cdTema: string,
        public stActivo: number,
        public isActive: boolean,
        public stMultiColor: boolean,
        public cdColor1: string,
        public cdColor2: string
    ) { }

}

export class AppSettingsVO {

    // constructor(
        public idConfiguracion: number;
        public resolucion: ResolutionVO;
        public tema: ThemeVO;
        public aplicacion: ApplicationVO;
        public logoMenuPrincipal: string;
        public logoMenuSecundario: string;
        public logoHeader: string;
        public logoIndex: string;
        public stActivo: number;
        public stMenuFijo: number;
        public stHeaderFijo: number;
        public stMenuDesplegable: number;
    // ) { }

}

export class Settings {

    constructor(
        public idConfiguracion: number,
        public name: string,
        public title: string,
        public tokenlenght: number,
        public moduleAccess: string,
        public logotipo: string,
        public isotipo: string,
        public imgMain: string,
        public application: ApplicationVO,
        public resolution: ResolutionVO,
        public theme: {
            menu: string;
            menuType: string;
            showMenu: boolean;
            navbarIsFixed: boolean;
            footerIsFixed: boolean;
            sidebarIsFixed: boolean;
            showSideChat: boolean;
            sideChatIsHoverable: boolean;
            skin: string;
            nameSkin: string
        }
    ) { }

}

export class InactivityVO {

    constructor(
        public isInactivity: boolean,
        public isPreinactivity: boolean,
        public isCloseInactivity: boolean,
        public minutesCounter: number,
        public secondsCounter: number,
        public zeroMinutes: string,
        public zeroSeconds: string,
        public TimerCounter: any,
        public TimeAwait: any
    ) { }

}

export interface LoaderState { show: boolean; }
