export class UserVO {

    constructor(
        public idUser: number,
        public username: string,
        public name: string,
        public profile: string,
        public audience: string,
        public create: Date,
        public expiration: Date,
        public inactivity: number,
        public pages: string,
        public caja: string,
        public deposito: string,
    ) { }

}
