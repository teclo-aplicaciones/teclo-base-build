export class MenuVO {

    constructor(
        public id: number,
        public menuTexto: string,
        public menuTextoEn: string,
        public menuSuperior: number,
        public menuUri: string,
        public menuIcono: string
    ) { }

}

export class Menu {
    constructor(
        public id: number,
        public title: string,
        public titleEn: string,
        public routerLink: string,
        public href: string,
        public icon: string,
        public target: string,
        public hasSubMenu: boolean,
        public parentId: number
    ) { }
}
