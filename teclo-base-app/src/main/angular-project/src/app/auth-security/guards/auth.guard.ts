import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { AuthService } from '@services/application/auth.service';
import { AppSettings } from '@config/settings.config';

@Injectable()
export class AuthGuard implements CanLoad {

  constructor(
    private authService: AuthService,
    private router: Router,
    private appSettings: AppSettings
    ) { }

  canLoad() {

    if ( this.authService.isAuthenticated() ) {
      this.router.navigate(['/app']);
      this.appSettings.isToken = true;
      return false;
    } else { return true; }

  }
}
