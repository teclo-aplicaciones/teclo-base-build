import { Injectable } from '@angular/core';
import { AuthService } from '@services/application/auth.service';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AppSettings } from '@config/settings.config';

@Injectable()
export class PageGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private appSettings: AppSettings,
    private router: Router
    ) { }

  canActivate() {
    if ( this.authService.isAuthenticated() ) {
      this.authService.getDecodedToken();
      this.appSettings.isToken = true;
      return true;
    } else {
      this.authService.goLogout();
      return false;
    }
  }

  canActivateChild(route: ActivatedRouteSnapshot): boolean {

    if ( !this.authService.isAuthenticated() ) { this.authService.goLogout(); }

    if ( route.url.length < 1 ) { this.router.navigate(['app/not-found']); }

    const current = route.url[0].path;
    const modules = this.appSettings.settings.moduleAccess;
    let pages = this.authService.getPayload('pages');

    if (modules !== '') { pages += ` ${modules}`; }

    if ( !pages.includes(current) ) {
      this.router.navigate(['app/unauthorized']);
    }

    return true;
  }

}
// console.log(this.authService.getPayload("exp"));
// console.log(this.authService.getPayload("inactivity"));
// console.log(this.authService.getPayload("name"));
// console.log(this.authService.getPayload("pages"));
// console.log(this.authService.getPayload("perfil"));
// console.log(this.authService.getPayload("sub"));
// console.log(this.authService.getPayload("ss"));
