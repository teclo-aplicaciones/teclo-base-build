import { Injectable } from '@angular/core';
import { WebstorageService } from '@services/application/webstorage.service';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest
} from '@angular/common/http'
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
import { AppSettings } from '@config/settings.config';
import { AppService } from '@services/application/app.service';
import { Environment } from '@environment/environment';
import { AuthService } from '@services/application/auth.service';


@Injectable({ providedIn: 'root' })
export class RequestInterceptor implements HttpInterceptor {

    private activeRequest = 0;

    constructor(
        private webstorageService: WebstorageService,
        private appService: AppService,
        private authService: AuthService,
        protected appSettings: AppSettings
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const token = this.webstorageService.getToken();

        if ( this.appSettings.isToken ) {
            this.authService.goRequest();
        }

        if (token) {
            request = request.clone({ headers: request.headers.set('X-Auth-Token', token) });
        }

        if ( !request.headers.has('Content-Type') ) {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json;charset=UTF-8') });
        }

        request = request.clone( {headers: request.headers.set('Accept', 'application/json')} );

        let displayLoader = true;

        for ( const skip of Environment.skipUris ) {
            if ( new RegExp(skip).test(request.url) ) {
                displayLoader = false;
                break;
            }
        }

        if ( displayLoader ) {

            if ( this.activeRequest === 0 ) { this.onStart(); }

            this.activeRequest++;

            return next.handle(request).pipe(
                map( (event: HttpEvent<any>) => event ),
                finalize( () => {
                    this.activeRequest--;
                    if ( this.activeRequest === 0 ) { this.onEnd(); }
                })
            );

        } else {
            return next.handle(request);
        }
    }

    private onStart(): void { this.appService.showLoader(); }

    private onEnd(): void { this.appService.hideLoader(); }
}
