import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest
   } from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AlertsService } from '@services/application/alerts.service';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(
        readonly alert: AlertsService
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError((error: any) => {
                if (error.status === 0) {
                    return this.handle0Error(request, next);
                }
                if (error.status === 401 && !request.url.includes('/login')) {
                    return this.handle401Error(request, next);
                }
                if (error.status === 500) {
                    return this.handle500Error(request, next);
                }
                if (error.status === 503) {
                  return this.handle503Error(request, next);
              }
                return throwError(error);
            })
        );
    }

    private handle0Error(request: HttpRequest<any>, next: HttpHandler) {
        this.alert.error(
            'Tiempo de conexión finalizado',
            'Parece que hay problemas con la conexión, intente más tarde'
        );
        return next.handle(request);
    }
    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        return next.handle(request);
    }
    private handle500Error(request: HttpRequest<any>, next: HttpHandler) {
        return next.handle(request);
    }

    private handle503Error(request: HttpRequest<any>, next: HttpHandler) {
      this.alert.error(
          'Servico no disponible',
          'Parece que hay problemas con la conexión al servicio, intente más tarde'
      );
      return next.handle(request);
  }

}
