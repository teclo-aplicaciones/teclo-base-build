import { Component, OnInit, ViewEncapsulation, ElementRef, Input, AfterViewInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MenuService } from '@services/application/menu.service';
import { AppSettings } from '@core/config/settings.config';
import { Settings } from '@models/app/application.model';
import { Menu } from '@models/app/menu.model';

@Component({
  selector: 'app-horizontal-menu',
  templateUrl: './horizontal-menu.component.html',
  styleUrls: ['./horizontal-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MenuService]
})
export class HorizontalMenuComponent implements OnInit, AfterViewInit {

  @Input('menuItems') menuItems: Array<Menu>;

  public settings: Settings;
  // public menuItems: Array<Menu>;

  constructor(
    public appSettings: AppSettings,
    private menuService: MenuService,
    private router: Router,
    private elementRef: ElementRef
  ) {
    this.settings = this.appSettings.settings;

    this.router.events.subscribe(event => {

      if (event instanceof NavigationEnd) {

        window.scrollTo(0, 0);

        const activeLink = this.menuService.getActiveLink(this.menuItems);

        this.menuService.setActiveLink(this.menuItems, activeLink);

        jQuery('.tooltip').tooltip('hide');
      }
    });
  }

  ngOnInit() {

    this.menuItems = this.appSettings.menu;

    const menuWrapper = this.elementRef.nativeElement.children[0];

    this.menuService.createMenu(this.menuItems, menuWrapper, 'horizontal');

    if (this.settings.theme.menuType === 'mini') {
      jQuery('.menu-item-link').tooltip();
    }
  }

  ngAfterViewInit() {

    const activeLink = this.menuService.getActiveLink(this.menuItems);

    this.menuService.setActiveLink(this.menuItems, activeLink);
  }

}
