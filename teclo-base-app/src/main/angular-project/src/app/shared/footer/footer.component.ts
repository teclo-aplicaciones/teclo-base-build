import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppSettings } from '@core/config/settings.config';
import { Settings } from '@models/app/application.model';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FooterComponent implements OnInit {

  public settings: Settings;

  constructor(
    public appSettings: AppSettings
  ) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {
  }

}
