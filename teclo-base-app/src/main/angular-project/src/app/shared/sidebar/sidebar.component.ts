import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppSettings } from '@core/config/settings.config';
import { Settings } from '@models/app/application.model';
import { MenuService } from '@services/application/menu.service';
import { Menu } from '@models/app/menu.model';
import { Assets } from '@environment/environment';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MenuService]
})
export class SidebarComponent implements OnInit {

  public settings: Settings;
  public menuItems: Array<Menu>;
  public assets = Assets;

  constructor(
    public appSettings: AppSettings,
    public menuService: MenuService
  ) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {

    if (sessionStorage['userMenuItems']) {

      const ids = JSON.parse(sessionStorage.getItem('userMenuItems'));
      const newArr = [];

      ids.forEach( (id: number) => {
        const newMenuItem = this.menuItems.filter(mail => mail.id === id);
        newArr.push(newMenuItem[0]);
      });

      this.menuItems = newArr;

    }

  }

  public closeSubMenus() {

    const menu = document.querySelector('#menu0');

    for (let i = 0; i < menu.children.length; i++) {

      const child = menu.children[i].children[1];

      if (child) {

        if (child.classList.contains('show')) {

          child.classList.remove('show');
          menu.children[i].children[0].classList.add('collapsed');

        }

      }

    }

  }

}
