import { Component, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { AppSettings } from '@core/config/settings.config';
import { Settings } from '@models/app/application.model';
import { MenuService, AuthService } from '@services/service.main';
import { MenuVO, Menu } from '@models/app/menu.model';
import { Assets } from '@environment/environment';
import { UserVO } from '@models/app/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MenuService],
  animations: [
    trigger('showInfo', [
      state('1', style({ transform: 'rotate(180deg)' })),
      state('0', style({ transform: 'rotate(0deg)' })),
      transition('1 => 0', animate('400ms')),
      transition('0 => 1', animate('400ms'))
    ])
  ]
})
export class HeaderComponent implements OnInit {

  public showHorizontalMenu = true;
  public showInfoContent = false;
  public settings: Settings;
  public menuItems: Menu[];
  public user: UserVO;
  public isMenuCollapsed: boolean;
  public assets = Assets;

  constructor(
    public appSettings: AppSettings,
    private menuService: MenuService,
    private authService: AuthService
  ) {

    this.settings = this.appSettings.settings;
    this.user = appSettings.user;

    if (this.appSettings.menu) {

      const activeLink = this.menuService.getActiveLink(this.menuItems);

      this.menuItems = this.appSettings.menu;
      this.appSettings.isLoadMenu = true;

      this.menuService.setActiveLink(this.menuItems, activeLink);

    } else {
      this.menuService.getMenuNavigation().subscribe((menus: Menu[]) => {

        const activeLink = this.menuService.getActiveLink(menus);

        this.menuItems = this.appSettings.menu;
        this.appSettings.menu = menus;
        this.appSettings.isLoadMenu = true;

        this.menuService.setActiveLink(menus, activeLink);

      });

    }
  }

  ngOnInit() {

    if (window.innerWidth <= 768) {
      this.showHorizontalMenu = false;
    }

  }

  public closeSubMenus() {

    const menu = document.querySelector('#menu0');

    if (menu) {

      let i = 0;

      while (i < menu.children.length) {

        const child = menu.children[i].children[1];

        if (child) {
          if (child.classList.contains('show')) {
            child.classList.remove('show');
            menu.children[i].children[0].classList.add('collapsed');
          }
        }

        i++;
      }
    }

  }

  goLogout(): void { this.authService.goLogout(); }

  @HostListener('window:resize')
  public onWindowResize(): void {

    if (window.innerWidth <= 768) {
      this.showHorizontalMenu = false;
      this.isMenuCollapsed = true;
    } else {
      this.showHorizontalMenu = true;
      this.isMenuCollapsed = false;
    }
  }

}
