import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppSettings } from '@config/settings.config';
import { UserVO } from '@models/app/user.model';
import { AuthService } from '@core/services/service.main';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserMenuComponent implements OnInit {

  public user: UserVO;
 
  constructor(
    protected appSettings: AppSettings,
    public authService: AuthService
  ) {
    this.user = appSettings.user;
  }

  ngOnInit() {}
}
