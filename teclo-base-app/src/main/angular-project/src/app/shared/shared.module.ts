
// ANGULAR MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// APP MODULES
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AppTranslateModule } from '@components/app-translate.module';
import { PipesModule } from '@pipes/pipes.module';
import { ServicePagesModule } from '@services/pages/service-pages.module';
import {SplitButtonModule} from 'primeng/splitbutton';

// COMPONENTS
import { HeaderComponent } from '@shared/header/header.component';
import { UserMenuComponent } from '@shared/user-menu/user-menu.component';
import { SidebarComponent } from '@shared/sidebar/sidebar.component';
import { VerticalMenuComponent } from '@shared/menu/vertical-menu/vertical-menu.component';
import { HorizontalMenuComponent } from '@shared/menu/horizontal-menu/horizontal-menu.component';
import { BreadcrumbComponent } from '@shared/breadcrumb/breadcrumb.component';
import { FooterComponent } from '@shared/footer/footer.component';
import { BackTopComponent } from '@shared/back-top/back-top.component';
import { PagesComponent } from '@pages/pages.component';
import { HomeComponent } from '@pages/home/home.component';
import { ComponentsModule } from '@components/components.module';
import { PrimengComponentsComponent } from '@pages/primeng-components/primeng-components.component';
import { InternalServerComponent } from '@shared/errors/internal-server/internal-server.component';
import { NotFoundComponent } from '@shared/errors/not-found/not-found.component';
import { UnauthorizedComponent } from '@shared/errors/unauthorized/unauthorized.component';

import { ElementosBaseComponent } from '@pages/elementosBase/elementosBase.component';

// ROUTES
import { RoutingPages } from '@pages/pages.routing';
// INITIALIZATION CONFIG
import { DirectivesPrimeNg } from '@directives/directives-primeng';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import {ToolbarModule} from 'primeng/toolbar';
import { ToggleButtonModule } from 'primeng';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    RoutingPages,
    ServicePagesModule,
    PipesModule,
    ComponentsModule,
    AppTranslateModule,
    DirectivesPrimeNg,
    ToolbarModule,
    SplitButtonModule
  ],
  declarations: [
    PagesComponent,
    HeaderComponent,
    UserMenuComponent,
    SidebarComponent,
    VerticalMenuComponent,
    HorizontalMenuComponent,
    BreadcrumbComponent,
    FooterComponent,
    BackTopComponent,
    NotFoundComponent,
    InternalServerComponent,
    UnauthorizedComponent,
    HomeComponent
   ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]
})
export class SharedModule { }
