import { Component, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  selector: 'app-internal-server',
  templateUrl: './internal-server.component.html',
  styleUrls: ['./internal-server.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InternalServerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
