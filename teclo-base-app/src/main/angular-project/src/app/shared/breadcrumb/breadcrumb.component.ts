import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, ActivatedRouteSnapshot, UrlSegment, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { AppSettings } from '@core/config/settings.config';
import { Settings } from '@models/app/application.model';

@Component({
    selector: 'app-breadcrumb',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './breadcrumb.component.html'
})
export class BreadcrumbComponent {

    public settings: Settings;
    public pageTitle: string;
    public breadcrumbs: {
        name: string;
        url: string
    }[] = [];
    public isVisibleBreadcrumb = true;

    constructor(
        public appSettings: AppSettings,
        public router: Router,
        public activatedRoute: ActivatedRoute,
        public title: Title
    ) {

        this.settings = this.appSettings.settings;

        this.router.events.subscribe(event => {

            if (event instanceof NavigationEnd) {

                if ( event.url === '/app/error' || event.url === '/app/unauthorized' ) {
                    this.isVisibleBreadcrumb = false;
                    return;
                }

                this.breadcrumbs = [];
                this.parseRoute( this.router.routerState.snapshot.root );
                this.pageTitle = '';

                this.breadcrumbs.forEach( breadcrumb => {
                    this.pageTitle += ` > ${breadcrumb.name}`;
                });

                this.title.setTitle(`${this.settings.name} ${this.pageTitle}` );
                this.isVisibleBreadcrumb = true;
            }
        });
    }

    parseRoute(node: ActivatedRouteSnapshot) {

        if (node.data.breadcrumb) {

            if ( node.url.length ) {

                let urlSegments: UrlSegment[] = [];
                node.pathFromRoot.forEach(routerState => {
                    urlSegments = urlSegments.concat(routerState.url);
                });

                const url = urlSegments.map(urlSegment => {
                    return urlSegment.path;
                }).join('/');

                this.breadcrumbs.push({
                    name: node.data.breadcrumb,
                    url: '/' + url
                });
            }
        }

        if ( node.firstChild ) {
            this.parseRoute(node.firstChild);
        }
    }

     public closeSubMenus() {

        const menu = document.querySelector('#menu0');

        if ( menu ) {

            let i = 0;

            while ( i < menu.children.length ) {

                const child = menu.children[i].children[1];

                if (child) {

                    if (child.classList.contains('show')) {
                        child.classList.remove('show');
                        menu.children[i].children[0].classList.add('collapsed');
                    }
                }

                i++;
            }
            // for (let i = 0; i < menu.children.length; i++) {

            //     const child = menu.children[i].children[1];

            //     if (child) {

            //         if (child.classList.contains('show')) {
            //             child.classList.remove('show');
            //             menu.children[i].children[0].classList.add('collapsed');
            //         }
            //     }
            // }
        }
    }
}
