import { Component, ViewEncapsulation } from '@angular/core';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { AppSettings } from '@config/settings.config';
import { Menu } from '@models/app/menu.model';
import { TranslateService, TranslationChangeEvent, LangChangeEvent } from '@ngx-translate/core';


@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FavoritesComponent {

  private translatefav = {};
  public favoriteModel: number[] = [];
  public favoriteSettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-secondary btn-block',
    dynamicTitleMaxItems: 0,
    displayAllSelectedText: true
  };
  public favoriteTexts: IMultiSelectTexts = {
    checkAll: 'Seleccionar todo',
    uncheckAll: 'Deseleccionar todo',
    checked: 'Elemento seleccionado',
    checkedPlural: 'Elementos seleccionados',
    searchPlaceholder: 'Filtrar acceso por...',
    defaultTitle: 'Selecciona tus accesos favoritos',
    allSelected: 'Todos los seleccionados',
  };
  public favoriteOptions: IMultiSelectOption[] = [];
  public menuItems: Menu[];
  public toggle: boolean;
  public favorites: Array<any> = [];

  constructor(
    public appSettings: AppSettings,
    private readonly translate: TranslateService
  ) {
    this.menuItems = this.appSettings.menu.filter(menu => menu.routerLink != null || menu.href != null);
    this.menuItems.forEach(item => {
      const menu = {
        id: item.id,
        name: item.title,
        routerLink: item.routerLink,
        href: item.href,
        icon: item.icon,
        target: item.target
      };
      this.favoriteOptions.push(menu);
    });
    if (sessionStorage.favorites) {
      this.favorites = JSON.parse(sessionStorage.getItem('favorites'));
      this.favorites.forEach(favorite => {
        this.favoriteModel.push(favorite.id);
      });
    }

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {

      const translations = event.translations.COMPONENTS.FAVORITES;

      this.favoriteTexts.checkAll = translations.CHECKALL;
      this.favoriteTexts.uncheckAll = translations.UNCHECKALL;
      this.favoriteTexts.checked = translations.CHECKED;
      this.favoriteTexts.checkedPlural = translations.CHECKEDPLURAL;
      this.favoriteTexts.searchPlaceholder = translations.SEARCHPLACEHOLDER;
      this.favoriteTexts.defaultTitle = translations.DEFAULTTITLE;
      this.favoriteTexts.allSelected = translations.ALLSELECTED;
    });
  }

  public onDropdownOpened() {
    this.toggle = true;
  }
  public onDropdownClosed() {
    this.toggle = false;
  }

  public onChange() {
    this.favorites.length = 0;
    this.favoriteModel.forEach(i => {
      const favorite = this.favoriteOptions.find(mail => mail.id === +i);
      this.favorites.push(favorite);
    });
    sessionStorage.setItem('favorites', JSON.stringify(this.favorites));
  }


}
