// ANGULAR MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
// COMPONENTS
import { FullscreenComponent } from '@components/fullscreen/fullscreen.component';
import { CalendarComponent } from '@components/calendar/calendar.component';
import { FavoritesComponent } from '@components/favorites/favorites.component';
import { MultilangsComponent } from '@components/multilangs/multilangs.component';
import { AppTranslateModule } from '@components/app-translate.module';
import { EnvIndicatorComponent } from '@components/env-indicator/env-indicator.component';
import { InactivityComponent } from '@components/inactivity/inactivity.component';
// INITIALIZATION CONFIG
import { DirectivesPrimeNg } from '@directives/directives-primeng';
import { ConfigAppComponent } from './config-app/config-app.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MultiselectDropdownModule,
    NgbModule,
    RouterModule,
    AppTranslateModule,
    DirectivesPrimeNg
  ],
  declarations: [
    FullscreenComponent,
    CalendarComponent,
    FavoritesComponent,
    MultilangsComponent,
    EnvIndicatorComponent,
    InactivityComponent,
    ConfigAppComponent
  ],
  exports: [
    FullscreenComponent,
    CalendarComponent,
    FavoritesComponent,
    MultilangsComponent,
    EnvIndicatorComponent,
    InactivityComponent,
    ConfigAppComponent
  ]
})
export class ComponentsModule { }
