import { Component, OnInit } from '@angular/core';
import { AppSettings } from '@config/settings.config';
import { AppService, WebstorageService, AlertsService } from '@services/service.main';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-env-indicator',
  templateUrl: './env-indicator.component.html',
  styleUrls: ['./env-indicator.component.scss']
})
export class EnvIndicatorComponent implements OnInit {

  public blinkOpacity = 1;
  public environt: any = new Object();
  public isVisible: boolean;

  constructor(
    private appSettings: AppSettings,
    readonly appService: AppService,
    readonly wss: WebstorageService,
    readonly alert: AlertsService
  ) { }

  ngOnInit() { this.configEnvironment(); }

  private configEnvironment() {

    const environtmp = this.appSettings.storageName;

    switch ( environtmp.ambient ) {
      case 'D':
        this.environt.ambient = 'Desarrollo';
        this.environt.color = '#FF9100';
        this.environt.border = '#E65100';
        this.isVisible = true;
        break;
      case 'Q':
        this.environt.ambient = 'QA';
        this.environt.color = '#00B8D4';
        this.environt.border = '#006064';
        this.isVisible = true;
        break;
      case 'P':
        this.environt.ambient = 'Producción';
        this.environt.color = '#00C853';
        this.environt.border = '#1B5E20';
        this.isVisible = false;
        break;
      default:
        this.environt.ambient = 'Indefinido';
        this.environt.color = '#969597';
        this.environt.border = '#55565a';
        this.isVisible = true;
    }

    if ( environtmp.base === 'O' ) {
      this.environt.base = 'Oracle';
    } else if ( environtmp.base === 'S' ) {
      this.environt.base = 'SQL Server';
    } else {
      this.environt.base = 'Indefinida';
    }
  }

  public updateEnvironment() {
    this.appService.getCdAmbiente().subscribe( (code: any) => {

      const environt = `${code.ambiente}_${location.href.split('/')[3]}`;

      if ( environt !== this.appSettings.storageName.ambientbase ) {

        const nameStorageOld = {...this.appSettings.storageName};
        const token = this.wss.getToken() ? this.wss.getToken().slice() : null;
        const config = {...this.wss.getConfiguration()};

        this.wss.setNameStorage(code.ambiente);

        if ( token ) {
          this.wss.setToken(token);
          this.wss.deleteStorage(nameStorageOld.token);
        }

        this.wss.setConfiguration(config);
        this.wss.setAmbient(code.ambiente);

        this.appSettings.settings.application.ambiente = code.ambiente;

        this.wss.deleteStorage(nameStorageOld.ambientbase);
        this.wss.deleteStorage(nameStorageOld.config);

        this.configEnvironment();

      } else {
        return;
      }

    }, (badrequest: HttpErrorResponse) => {
      if ( badrequest.status === 404 ) {
        this.alert.error(
          'No se encuentra el ambiente',
          'Al parecer no se ha definido ningún ambiente para esta aplicación')
      }
    });
  }

}
