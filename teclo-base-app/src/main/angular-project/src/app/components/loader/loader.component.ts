import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { AppService } from '@services/service.main';
import { LoaderState } from '@models/app/application.model';
import { debounce, takeWhile } from 'rxjs/operators';


@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, OnDestroy {

  public show = false;
  private subscription: Subscription;

  constructor( private appService: AppService ) { }

  ngOnInit() {
    this.subscription = this.appService.loaderState
      .pipe(
        debounce( () => timer(200)),
        takeWhile( res => res.show <= true )
      )
      .subscribe((state: LoaderState) => { this.show = state.show; });
  }

  ngOnDestroy() { this.subscription.unsubscribe(); }
}
