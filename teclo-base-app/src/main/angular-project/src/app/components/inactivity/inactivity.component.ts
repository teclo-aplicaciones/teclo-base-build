import { Component, ViewChild, ElementRef, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { AppSettings } from '@config/settings.config';
import { AuthService, WebstorageService } from '@services/service.main';
import { InactivityVO } from '@models/app/application.model';
import {
  fadeInRightOnEnterAnimation,
  fadeOutRightOnLeaveAnimation
} from 'angular-animations';
import { Observable, timer, Subscription } from 'rxjs';

@Component({
  selector: 'app-inactivity',
  templateUrl: './inactivity.component.html',
  styleUrls: ['./inactivity.component.scss'],
  animations: [
    fadeInRightOnEnterAnimation({ anchor: 'enter', duration: 700 }),
    fadeOutRightOnLeaveAnimation({ anchor: 'leave', duration: 500 })
  ]
})
export class InactivityComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('sesionProgress', {static: false}) sesionProgress: ElementRef;

  public inactivity: InactivityVO;
  public progressCounter: number;
  private IntervalBonus: any;
  private IntervalSession: any;
  private TimerInactivity: Subscription;
  private TimerSession: Subscription;

  constructor(
    public appSettings: AppSettings,
    public readonly authService: AuthService,
    private readonly wss: WebstorageService
  ) {
    this.inactivity = appSettings.inactivity;
  }

  ngOnInit() {

    this.authService.Request.subscribe((value: boolean) => {

      if (this.TimerInactivity) {
        this.TimerInactivity.unsubscribe();
      }

      if (this.wss.getToken()) {

        this.inactivity.TimeAwait = clearTimeout(this.inactivity.TimeAwait);
        this.inactivity.TimeAwait = 0;
        this.inactivity.minutesCounter = 0;
        this.inactivity.secondsCounter = 0;
        this.inactivity.isPreinactivity = false;
        this.inactivity.isInactivity = false;
        this.inactivity.zeroMinutes = '';
        this.inactivity.zeroSeconds = '';

        const timeInactivity = ((this.authService.getPayload('inactivity') || 1) * 60) * 1000;
        // const timeInactivity = (1 * 60) * 1000;
        const timePreInactivity = this.authService.getPayload('preinactivity') || 50;
        const timeawait = timeInactivity - (timePreInactivity * 1000);

        this.setMinuteSeconds(timePreInactivity);

        this.inactivity.TimeAwait = setTimeout(() => {

          this.wss.setStatusRequest(false);
          this.inactivity.isPreinactivity = true;
          this.inactivity.TimerCounter = timer(1000, 1000);

          this.TimerInactivity = this.inactivity.TimerCounter.subscribe((num: number) => {

            if (!this.authService.isTokenExpired() || this.wss.getToken()) {

              if (!this.wss.getStatusRequest()) {
                if (this.inactivity.secondsCounter !== 1) {
                  this.inactivity.secondsCounter--;
                  if (this.inactivity.secondsCounter < 10) {
                    this.inactivity.zeroSeconds = '0';
                  }
                  else { this.inactivity.zeroSeconds = ''; }
                } else {
                  if (this.inactivity.minutesCounter !== 0) {
                    this.inactivity.minutesCounter--;
                    if (this.inactivity.minutesCounter < 10) {
                      this.inactivity.zeroMinutes = '0';
                    }
                    else { this.inactivity.zeroSeconds = ''; }
                    this.inactivity.zeroSeconds = '';
                    this.inactivity.secondsCounter = 59;
                  } else {
                    this.TimerInactivity.unsubscribe();
                    this.inactivity.isPreinactivity = false;
                    this.inactivity.isInactivity = true;
                  }
                }
              } else {
                this.authService.goRequest();
              }

            } else {
              this.authService.goLogout();
            }

          });

        }, timeawait);

      } else {
        this.authService.goLogout();
      }

    });
  }

  public refreshSesion() {
    this.authService.refreshToken();
  }

  public initProgress() {

    this.progressCounter = 10;
    this.sesionProgress.nativeElement.style.width = '100%';

    this.IntervalBonus = setInterval(() => {

      if (!this.wss.getStatusRequest()) {
        this.progressCounter--;
        if (this.progressCounter < 1) {
          this.appSettings.inactivity.isInactivity = false;
          this.appSettings.inactivity.isCloseInactivity = true;
          this.authService.breakTimer(this.IntervalBonus);
          this.authService.goLogout();
        } else {
          this.sesionProgress.nativeElement.style.width = `${this.progressCounter}0%`;
        }
      } else {
        if (this.wss.getToken()) {
          this.browsing();
        } else {
          this.authService.breakTimer(this.IntervalBonus);
          this.appSettings.inactivity.isInactivity = false;
          this.authService.goLogout();
        }
      }

    }, 1000);
  }

  public browsing() {
    this.appSettings.inactivity.isInactivity = false;
    this.authService.refreshToken();
    this.authService.breakTimer(this.IntervalBonus);
  }

  private setMinuteSeconds(time: number) {
    if (time <= 60) {
      this.inactivity.minutesCounter = 0;
      this.inactivity.secondsCounter = time;
    } else {
      this.inactivity.zeroMinutes = '0';
      for (let i = 0; i <= time; i++) {
        if (i === 60) {
          this.inactivity.minutesCounter++;
          time = time - 60;
          if (time < 60) {
            this.inactivity.secondsCounter = time;
          }
          i = 0;
        }
      }
    }
  }

  ngAfterViewInit() {
    this.authService.Request.next(true);
    this.IntervalSession = timer(1000, 1000);
    this.TimerSession = this.IntervalSession.subscribe( (num: number) => {
      if (!this.wss.getToken()) {
        this.authService.goLogout();
      }
    });
  }

  ngOnDestroy() {
    if (this.TimerInactivity) { this.TimerInactivity.unsubscribe(); }
    if (this.TimerSession) { this.TimerSession.unsubscribe(); }
    clearTimeout(this.inactivity.TimeAwait);
    this.inactivity.TimeAwait = 0;
  }
}
