import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MenuService } from '@services/service.main';
import { AppSettings } from '@config/settings.config';

@Component({
  selector: 'app-multilangs',
  templateUrl: './multilangs.component.html',
  styleUrls: ['./multilangs.component.scss']
})
export class MultilangsComponent implements OnInit {

  public flagSelected = 'flag-icon-mx';

  constructor(
    private translate: TranslateService,
    private menuService: MenuService,
    private appSettings: AppSettings
  ) {
    this.translate.use('es');
  }

  ngOnInit() { }

  changeLanguage(lang: string, icon: string) {
    this.flagSelected = icon;
    this.translate.use(lang);

    let menu: string;
    let menuElement: any;

    if ( this.appSettings.settings.theme.menu === 'vertical' ) {
      menu = 'vertical';
      menuElement = document.getElementById('vertical-menu');
      menuElement.innerHTML = '';
    } else {
      menu = 'horizontal';
      menuElement = document.getElementById('horizontal-menu');
      menuElement.innerHTML = '';
    }

    setTimeout(() => {
      this.menuService.createMenu(this.appSettings.menu, menuElement, menu);
      this.menuService.showActiveSubMenu(this.appSettings.menu);
      const activeLink = this.menuService.getActiveLink(this.appSettings.menu);
      this.menuService.setActiveLink(this.appSettings.menu, activeLink);
    }, 100);
  }

}
