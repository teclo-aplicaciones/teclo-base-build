import { ModuleWithProviders } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard, PageGuard } from '@services/service.main';

export const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'app', canActivate: [PageGuard], loadChildren: () => import('app/pages/pages.module').then(m => m.PagesModule) },
  { path: 'login', canLoad: [AuthGuard], loadChildren: () => import('app/start-pages/login/login.module').then(m => m.LoginModule) },
  { path: 'register', canLoad: [AuthGuard], loadChildren: () => import('app/start-pages/register/register.module').then(m => m.RegisterModule) },
  { path: '**', canActivate: [PageGuard], loadChildren: () => import('app/pages/pages.module').then(m => m.PagesModule) }
  
];

export const RoutingMain: ModuleWithProviders = RouterModule.forRoot(routes, {
  preloadingStrategy: PreloadAllModules,
  useHash: true
});
