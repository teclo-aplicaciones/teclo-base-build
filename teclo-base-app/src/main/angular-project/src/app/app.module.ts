// ANGULAR MODULES
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule } from '@angular/common/http';
// APP MODULES
import { ServiceMainModule } from '@services/service-main.module';
import { StorageServiceModule } from 'angular-webstorage-service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppTranslateModule } from '@components/app-translate.module';
// COMPONENTS
import { AppComponent } from '@core/app.component';
import { LoaderComponent } from '@components/loader/loader.component';
// ROUTES
import { RoutingMain } from '@core/app.routing';
// INITIALIZATION CONFIG
import { AppSettings } from '@config/settings.config';
import { AppValidators } from '@config/validators.config';

@NgModule({
  declarations: [
    AppComponent, 
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RoutingMain,
    ServiceMainModule,
    StorageServiceModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      },
      isolate: false
    }),
    AppTranslateModule.forRoot()
    
  ],
  providers: [
    AppSettings,
    AppValidators
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
