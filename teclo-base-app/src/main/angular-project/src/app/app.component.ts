import { Component, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { AppSettings } from '@core/config/settings.config';
import { WebstorageService } from '@services/service.main';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements AfterViewInit {

  constructor(
    public appSettings: AppSettings,
    private readonly wss: WebstorageService,
    private readonly router: Router
  ) {

    wss.checkStorage();

    router.events.pipe(filter((event: NavigationEnd) => {
      return (event instanceof NavigationEnd);
    })).subscribe((event: NavigationEnd) => {
      if (appSettings.isToken && event.urlAfterRedirects === '/login') {
        window.location.reload();
        return false;
      }
    });
  }

  ngAfterViewInit() {
    document.getElementById('preloader').style.display = 'none';
  }

}
