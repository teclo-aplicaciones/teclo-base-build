import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { TooltipModule } from 'primeng/tooltip';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SliderModule } from 'primeng/slider';
import { AccordionModule } from 'primeng/accordion';
import { PaginatorModule } from 'primeng/paginator';
import { PickListModule } from 'primeng/picklist';
import { FieldsetModule } from 'primeng/fieldset';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PanelModule } from 'primeng/panel';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { SidebarModule } from 'primeng/sidebar';
import { DialogModule } from 'primeng/dialog';
import { PasswordModule } from 'primeng/password';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { ChipsModule } from 'primeng/chips';
import { ListboxModule } from 'primeng/listbox';
import { RatingModule } from 'primeng/rating';
import { SpinnerModule } from 'primeng/spinner';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { OrderListModule } from 'primeng/orderlist';
import { CarouselModule } from 'primeng/carousel';
import { LightboxModule } from 'primeng/lightbox';
import { StepsModule } from 'primeng/steps';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ProgressBarModule } from 'primeng/progressbar';
import {ToastModule} from 'primeng/toast';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {FileUploadModule} from 'primeng/fileupload';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {DragDropModule} from 'primeng/dragdrop';


export const DirectivesPrimeNg = [
    CalendarModule,
    DropdownModule,
    MultiSelectModule,
    TooltipModule,
    CheckboxModule,
    RadioButtonModule,
    SelectButtonModule,
    SliderModule,
    AccordionModule,
    PaginatorModule,
    PickListModule,
    FieldsetModule,
    ScrollPanelModule,
    PanelModule,
    OverlayPanelModule,
    SidebarModule,
    DialogModule,
    PasswordModule,
    InputSwitchModule,
    InputTextModule,
    ChipsModule,
    ListboxModule,
    RatingModule,
    SpinnerModule,
    TabViewModule,
    TableModule,
    OrderListModule,
    CarouselModule,
    LightboxModule,
    StepsModule,
    PanelMenuModule,
    ProgressBarModule,
    ToastModule,
    MessageModule,
    MessagesModule,
    FileUploadModule,
    ConfirmDialogModule,
    DragDropModule
];
