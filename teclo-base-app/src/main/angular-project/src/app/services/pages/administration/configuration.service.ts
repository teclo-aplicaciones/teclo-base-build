import { Injectable } from '@angular/core';
import { AlertsService } from '@services/service.main';


@Injectable()
export class ConfigurationService {

  constructor(
    readonly alert: AlertsService
  ) { this.testService(); }

  public testService() {
     this.alert.toast('Servicio ejecutado', 'success');
  }
}
