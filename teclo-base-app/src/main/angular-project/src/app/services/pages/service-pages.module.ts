// ANGULAR MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// SERVICES
import {
  ConfigurationService,
  UserService,
  ProfileService
} from '@services/pages/service.pages';
// INTERCEPTORS
import { RequestInterceptor } from '@security/interceptors/request.interceptor';
import { ErrorInterceptor } from '@security/interceptors/error.interceptor';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ConfigurationService,
    UserService,
    ProfileService,
    RequestInterceptor,
    ErrorInterceptor,
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ]
})
export class ServicePagesModule { }
