// ANNGULAR MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// SERVICES
import {
  AuthGuard,
  PageGuard,
  AuthService,
  AppService,
  MenuService,
  WebstorageService,
  AlertsService,
  AngularService
} from '@services/service.main';
// INTERCEPTORS
import { RequestInterceptor } from '@security/interceptors/request.interceptor';
import { ErrorInterceptor } from '@security/interceptors/error.interceptor';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [],
  providers: [
    AuthGuard,
    PageGuard,
    AuthService,
    AppService,
    MenuService,
    WebstorageService,
    AlertsService,
    AngularService,
    RequestInterceptor,
    ErrorInterceptor,
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ]
})
export class ServiceMainModule { }
