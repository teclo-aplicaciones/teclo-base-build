import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Environment } from '@environment/environment';
import { Settings, AppSettingsVO, LoaderState, ThemeVO } from '@models/app/application.model';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AppSettings } from '@config/settings.config';


@Injectable({providedIn: 'root'})
export class AppService {

  readonly API_URL = Environment.urlWs;
  readonly POST_HEADER = new HttpHeaders({ 'Content-Type': 'application/json;charset=UTF-8' });

  private loaderSubject = new Subject<LoaderState>();

  public loaderState = this.loaderSubject.asObservable();

  constructor(
    private http: HttpClient,
    private appSettings: AppSettings
  ) { }

  public getCdAmbiente(): any {
    return this.http.get(`${this.API_URL}/aplicacion/ambiente`)
    .pipe( map( (response: any) => {
      return response;
    }));
  }

  public getConfiguration(): any {
     return this.http.get<AppSettingsVO>(`${this.API_URL}/aplicacion/configuraciones`)
      .pipe( map( (config: AppSettingsVO) => {

        const settings = new Settings(
          config.aplicacion.idAplicacion,
          config.aplicacion.nombre,
          config.aplicacion.descripcion,
          0,
          '',
          `data:image/png;base64,${config.logoMenuPrincipal}` || null,
          `data:image/png;base64,${config.logoHeader}` || null,
          `data:image/png;base64,${config.logoIndex}` || null,
          {
            idAplicacion: config.aplicacion.idAplicacion,
            codigo: config.aplicacion.codigo,
            nombre: config.aplicacion.nombre,
            descripcion: config.aplicacion.descripcion,
            ambiente: config.aplicacion.ambiente || 'U_U'
          },
          {
            idResolucion: config.resolucion.idResolucion,
            cdResolucion: config.resolucion.cdResolucion,
            nbResolucion: config.resolucion.nbResolucion,
            nuPixelesBase: config.resolucion.nuPixelesBase,
            txResolucion: config.resolucion.nuPixelesBase,
            stActivo: config.resolucion.stActivo
          },
          {
            menu: 'vertical',
            menuType: 'default',
            showMenu: true,
            navbarIsFixed: config.stHeaderFijo === 1 ? true : false,
            footerIsFixed: false,
            sidebarIsFixed: config.stMenuFijo === 1 ? true : false,
            showSideChat: false,
            sideChatIsHoverable: true,
            skin: config.tema.cdTema || 'tema-tecloLight',
            nameSkin: config.tema.nbTema || 'TECLO-LIGHT'
          }
        );
        return settings;
      }));
  }

  public getThemes(): any {
    return this.http.get<AppSettingsVO>(`${this.API_URL}/aplicacion/configuraciones`)
    .pipe( map( (thema: AppSettingsVO) => {
        this.appSettings.configurationApp.tema = thema.tema;
        return thema;
      }));
  }

  public showLoader() {
    this.loaderSubject.next({ show: true } as LoaderState);
  }

  public hideLoader() {
    this.loaderSubject.next({ show: false } as LoaderState);
  }

}
