import { Location } from '@angular/common';
import { Injectable, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { Environment } from '@environment/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { MenuVO, Menu } from '@models/app/menu.model';
import { AppSettings } from '@config/settings.config';
import { WebstorageService } from '@services/application/webstorage.service';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class MenuService {

  readonly API_URL = Environment.urlWs;

  constructor(
    private location: Location,
    private renderer2: Renderer2,
    private router: Router,
    private http: HttpClient,
    private appSettings: AppSettings,
    private wss: WebstorageService,
    private translate: TranslateService
  ) { }

  public getMenuNavigation(): any {

    return this.http.get<MenuVO[]>(`${this.API_URL}/login/menus`)
      .pipe(map((menus: MenuVO[]) => {

        let menu: Menu;
        let moduleAccess = '';

        const array: Menu[] = [];

        array.push(new Menu(
          0, 'Inicio', 'Home',
          '/app/main', null,
          'home', null, false, 0)
        );

        for (const m of menus) {

          if (m.menuSuperior === 0) {
            menus.map(sm => {
              if (m.id === sm.menuSuperior) {
                if (!m.menuUri || m.menuUri.indexOf(' ') !== -1) {
                  sm.menuUri = 'app/unauthorized';
                } else {
                  sm.menuUri = `/app/${m.menuUri.toLowerCase()}${sm.menuUri}`;
                  if (!moduleAccess.includes(m.menuUri.toLowerCase())) {
                    moduleAccess += ` /${m.menuUri.toLowerCase()}`;
                  }
                }
              }
              return sm;
            });
          }

          if (m.menuUri === null) { m.menuUri = ' '; }

          menu = new Menu(
            m.id, m.menuTexto, m.menuTextoEn || null,
            m.menuUri.indexOf('/') !== -1 ? m.menuUri : null,
            null, m.menuIcono, null,
            m.menuSuperior === 0 ? true : false,
            m.menuSuperior
          );

          if (menu.icon === null || menu.icon.indexOf('fa') < 0) {
            menu.icon = 'circle-o';
          } else {
            if (menu.icon.indexOf('fa') !== -1) { menu.icon = menu.icon.split('-')[1]; }
          }

          array.push(menu);
        }

        this.appSettings.settings.moduleAccess = moduleAccess;
        this.wss.setConfiguration(this.appSettings.settings);

        return array;
      }));
  }

  public createMenu(menu: Array<Menu>, nativeElement: Element, type: string) {

    if (type === 'vertical') { this.createVerticalMenu(menu, nativeElement); }

    if (type === 'horizontal') { this.createHorizontalMenu(menu, nativeElement); }

  }

  public createVerticalMenu(menu: Array<Menu>, nativeElement: Element) {

    const menu0 = this.renderer2.createElement('div');

    this.renderer2.setAttribute(menu0, 'id', 'menu0');

    menu.forEach(menuItem => {

      if (menuItem.parentId === 0) { // Comprueba menuItem.parentId = menuSuperior
        const subMenu = this.createVerticalMenuItem(menu, menuItem);
        this.renderer2.appendChild(menu0, subMenu);
      }

    });

    this.renderer2.appendChild(nativeElement, menu0);

  }

  public createHorizontalMenu(menu: Array<Menu>, nativeElement: any) {

    const nav = this.renderer2.createElement('div');
    const ul = this.renderer2.createElement('ul');

    this.renderer2.setAttribute(nav, 'id', 'navigation');
    this.renderer2.addClass(ul, 'menu');
    this.renderer2.appendChild(nav, ul);

    menu.forEach(menuItem => {

      if (menuItem.parentId === 0) { // Comprueba menuItem.parentId = menuSuperior
        const subMenu = this.createHorizontalMenuItem(menu, menuItem);
        this.renderer2.appendChild(ul, subMenu);
      }

    });

    this.renderer2.appendChild(nativeElement, nav);

  }

  public createVerticalMenuItem(menu: Array<Menu>, menuItem: Menu) {

    const div = this.renderer2.createElement('div');

    this.renderer2.addClass(div, 'card');

    const link = this.renderer2.createElement('a');

    this.renderer2.addClass(link, 'menu-item-link');
    this.renderer2.addClass(link, 'a-10ms');
    this.renderer2.addClass(link, 'fadeIn');
    this.renderer2.setAttribute(link, 'data-toggle', 'tooltip');
    this.renderer2.setAttribute(link, 'data-placement', 'right');
    this.renderer2.setAttribute(link, 'data-animation', 'false');

    this.renderer2.setAttribute(
      link,
      'data-container',
      '.vertical-menu-tooltip-place'
    );

    if (this.translate.currentLang === 'es') {
      this.renderer2.setAttribute(link, 'data-original-title', menuItem.title); // Setea el menuItem.title = menuTexto
    } else if (this.translate.currentLang === 'en') {
      this.renderer2.setAttribute(link, 'data-original-title', menuItem.titleEn); // Setea el menuItem.title = menuTexto
    }

    const icon = this.renderer2.createElement('i');

    this.renderer2.addClass(icon, 'fa');
    this.renderer2.addClass(icon, 'fa-' + menuItem.icon); // Setea el menuItem.icon = menuIcono
    this.renderer2.appendChild(link, icon);

    const span = this.renderer2.createElement('span');

    this.renderer2.addClass(span, 'menu-title');
    this.renderer2.appendChild(link, span);

    let menuText: string;

    if (this.translate.currentLang === 'es') {
      menuText = this.renderer2.createText(menuItem.title); // Setea el menuItem.title = menuTexto
    } else if (this.translate.currentLang === 'en') {
      menuText = this.renderer2.createText(menuItem.titleEn); // Setea el menuItem.title = menuTexto
    }

    this.renderer2.appendChild(span, menuText);
    this.renderer2.setAttribute(link, 'id', 'link' + menuItem.id); // Setea el menuItem.id = id
    this.renderer2.addClass(link, 'transition');
    this.renderer2.appendChild(div, link);

    if (menuItem.routerLink) { // Comprueba si existe menuItem.routerLink = menuUri
      this.renderer2.listen(link, 'click', () => {
        this.router.navigate([menuItem.routerLink]); // Setea el menuItem.routerLink = menuUri
        this.setActiveLink(menu, link);
        this.closeOtherSubMenus(div);
      });
    }

    if (menuItem.href) { // Comprueba si existe menuItem.href = href
      this.renderer2.setAttribute(link, 'href', menuItem.href); // Setea el menuItem.href = href
    }

    if (menuItem.target) { // Comprueba si existe menuItem.target = target
      this.renderer2.setAttribute(link, 'target', menuItem.target); // Comprueba si existe menuItem.target = target
    }

    if (menuItem.hasSubMenu) { // Comprueba si existe menuItem.hasSubMenu = hasSubMenu
      this.renderer2.addClass(link, 'collapsed');

      const caret = this.renderer2.createElement('b');

      this.renderer2.addClass(caret, 'fa');
      this.renderer2.addClass(caret, 'fa-angle-up');
      this.renderer2.appendChild(link, caret);
      this.renderer2.setAttribute(link, 'data-toggle', 'collapse');
      this.renderer2.setAttribute(link, 'href', '#collapse' + menuItem.id); // Setea el menuItem.id = id

      const collapse = this.renderer2.createElement('div');

      this.renderer2.setAttribute(collapse, 'id', 'collapse' + menuItem.id); // Setea el menuItem.id = id

      this.renderer2.setAttribute(
        collapse,
        'data-parent',
        '#menu' + menuItem.parentId // Setea el menuItem.parentId = menuSuperior
      );

      this.renderer2.addClass(collapse, 'collapse');
      this.renderer2.appendChild(div, collapse);
      this.createSubMenu(menu, menuItem.id, collapse, 'vertical'); // Setea el menuItem.id = id
    }

    return div;
  }

  public createHorizontalMenuItem(menu: Array<Menu>, menuItem: Menu) {

    const li = this.renderer2.createElement('li');

    this.renderer2.addClass(li, 'menu-item');

    const link = this.renderer2.createElement('a');

    this.renderer2.addClass(link, 'menu-item-link');
    this.renderer2.setAttribute(link, 'data-toggle', 'tooltip');
    this.renderer2.setAttribute(link, 'data-placement', 'top');
    this.renderer2.setAttribute(link, 'data-animation', 'false');

    this.renderer2.setAttribute(
      link,
      'data-container',
      '.horizontal-menu-tooltip-place'
    );

    if (this.translate.currentLang === 'es') {
      this.renderer2.setAttribute(link, 'data-original-title', menuItem.title); // Setea el menuItem.title = menuTexto
    } else if (this.translate.currentLang === 'en') {
      this.renderer2.setAttribute(link, 'data-original-title', menuItem.titleEn); // Setea el menuItem.title = menuTexto
    }

    const icon = this.renderer2.createElement('i');

    this.renderer2.addClass(icon, 'fa');
    this.renderer2.addClass(icon, 'fa-' + menuItem.icon); // Setea menuItem.icon = menuIcono
    this.renderer2.appendChild(link, icon);

    const span = this.renderer2.createElement('span');

    this.renderer2.addClass(span, 'menu-title');
    this.renderer2.appendChild(link, span);

    let menuText: string;

    if (this.translate.currentLang === 'es') {
      menuText = this.renderer2.createText(menuItem.title); // Setea el menuItem.title = menuTexto
    } else if (this.translate.currentLang === 'en') {
      menuText = this.renderer2.createText(menuItem.titleEn); // Setea el menuItem.title = menuTexto
    }

    this.renderer2.appendChild(span, menuText);
    this.renderer2.appendChild(li, link);
    this.renderer2.setAttribute(link, 'id', 'link' + menuItem.id);  // Setea menuItem.id = id
    this.renderer2.addClass(link, 'transition');

    if (menuItem.routerLink) { // Comprueba el menuItem.routerLink = menuUri
      this.renderer2.listen(link, 'click', () => {
        this.router.navigate([menuItem.routerLink]); // Setea el menuItem.routerLink = menuUri
        this.setActiveLink(menu, link);
      });
    }
    if (menuItem.href) { // Comprueba el menuItem.href = href
      this.renderer2.setAttribute(link, 'href', menuItem.href); // Setea el menuItem.href = href
    }

    if (menuItem.target) {
      this.renderer2.setAttribute(link, 'target', menuItem.target); // Setea el menuItem.target = target
    }

    if (menuItem.hasSubMenu) { // Setea el menuItem.hasSubMenu = hasSubMenu
      this.renderer2.addClass(li, 'menu-item-has-children');

      const subMenu = this.renderer2.createElement('ul');

      this.renderer2.addClass(subMenu, 'sub-menu');
      this.renderer2.appendChild(li, subMenu);
      this.createSubMenu(menu, menuItem.id, subMenu, 'horizontal'); // Setea el menuItem.id = id
    }

    return li;
  }

  private createSubMenu(menu: Array<Menu>, menuItemId: number, parentElement: any, type: string) {

    const menus = menu.filter(item => item.parentId === menuItemId); // Filtra por item.parentId = menuSuperior

    menus.forEach(menuItem => {

      let subMenu = null;

      if (type === 'vertical') {
        subMenu = this.createVerticalMenuItem(menu, menuItem);
      }

      if (type === 'horizontal') {
        subMenu = this.createHorizontalMenuItem(menu, menuItem);
      }

      this.renderer2.appendChild(parentElement, subMenu);
    });

  }

  private closeOtherSubMenus(elem: any) {

    const children = this.renderer2.parentNode(elem).children;

    for (let i = 0; i < children.length; i++) {

      const child = this.renderer2.nextSibling(children[i].children[0]);

      if (child) {
        this.renderer2.addClass(children[i].children[0], 'collapsed');
        this.renderer2.removeClass(child, 'show');
      }
    }
  }

  public getActiveLink(menu: Array<Menu>) {

    const url = this.location.path();
    const routerLink = url; //  url.substring(1, url.length);
    const activeMenuItem = menu.filter(item => item.routerLink === routerLink); // Filtra item.routerLink = menuUri

    if (activeMenuItem[0]) {
      const activeLink = document.querySelector('#link' + activeMenuItem[0].id);
      return activeLink;
    }

    return false;
  }

  public setActiveLink(menu: Array<Menu>, link: any) {
    if (link) {
      menu.forEach(menuItem => {

        const activeLink = document.querySelector('#link' + menuItem.id); // Setea el menuItem.id = id

        if (activeLink) {
          if (activeLink.classList.contains('active-link')) {
            activeLink.classList.remove('active-link');
          }
        }
      });

      this.renderer2.addClass(link, 'active-link');
    }
  }

  public showActiveSubMenu(menu: Array<Menu>) {

    const url = this.location.path();
    const routerLink = url; // url.substring(1, url.length);
    const activeMenuItem = menu.filter(item => item.routerLink === routerLink); // Filtra item.routerLink = menuUri

    if (activeMenuItem[0]) {

      const activeLink = document.querySelector('#link' + activeMenuItem[0].id);
      let parent = this.renderer2.parentNode(activeLink);

      while (this.renderer2.parentNode(parent)) {

        parent = this.renderer2.parentNode(parent);

        if (parent.className === 'collapse') {
          const parentMenu = menu.filter(
            item => item.id === activeMenuItem[0].parentId // Setea parentId = menuSuperior
          );
          const activeParentLink = document.querySelector(
            '#link' + parentMenu[0].id
          );

          this.renderer2.removeClass(activeParentLink, 'collapsed');
          this.renderer2.addClass(parent, 'show');
        }

        if (parent.classList.contains('menu-wrapper')) {
          break;
        }
      }
    }
  }

  public addNewMenuItem(menu: Array<Menu>, newMenuItem: Menu, type: string) {

    menu.push(newMenuItem);

    if (newMenuItem.parentId !== 0) {

      const parentMenu = menu.filter(item => item.id === newMenuItem.parentId); // Filtra newMenuItem.parentId = menuSuperior

      if (parentMenu.length) {
        if (!parentMenu[0].hasSubMenu) {
          parentMenu[0].hasSubMenu = true;
        }
      }
    }

    let menuWrapper = null;

    if (type === 'vertical') {
      menuWrapper = document.getElementById('vertical-menu');
    }

    if (type === 'horizontal') {
      menuWrapper = document.getElementById('horizontal-menu');
    }

    while (menuWrapper.firstChild) {
      menuWrapper.removeChild(menuWrapper.firstChild);
    }

    this.createMenu(menu, menuWrapper, type);
  }
}
