import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Settings, StorageNameVO } from '@models/app/application.model';
import { AppSettings } from '@config/settings.config';
import { AppService } from '@services/application/app.service';


@Injectable()
export class WebstorageService {

  private code = 'App';

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: WebStorageService,
    protected appSettings: AppSettings,
    protected appService: AppService
  ) { }

  public setNameStorage(ambient: string): void {

    const context = `${location.href.split('/')[3]}`;

    this.appSettings.storageName = new StorageNameVO(
      ambient.split('_')[0],
      ambient.split('_')[1],
      `${ambient}_${context}`,
      `${ambient}_token${this.code}_${context}`,
      `${ambient}_statusinactivity${this.code}_${context}`,
      `${ambient}_statusrequest${this.code}_${context}`,
      `${ambient}_config${this.code}_${context}`,
      `${ambient}_configtmp${this.code}_${context}`,
      `${ambient}_wishlist${this.code}_${context}`
    );

  }

  public setToken(token: string): void {
    this.localStorage.set(this.appSettings.storageName.token, token);
  }

  public getToken(): any {
    if (this.appSettings.storageName) {
      return this.localStorage.get(this.appSettings.storageName.token);
    } else {
      return null;
    }
  }

  public setStatusInactivity(status: boolean): void {
    this.localStorage.set(this.appSettings.storageName.statusinactivity, status);
  }

  public getStatusInactivity(): any {
    if (this.appSettings.storageName) {
      return this.localStorage.get(this.appSettings.storageName.statusinactivity);
    } else {
      return null;
    }
  }

  public setStatusRequest(status: boolean): void {
    this.localStorage.set(this.appSettings.storageName.statusrequest, status);
  }

  public getStatusRequest(): any {
    if (this.appSettings.storageName) {
      return this.localStorage.get(this.appSettings.storageName.statusrequest);
    } else {
      return null;
    }
  }

  public setConfiguration(config: Settings): void {
    this.localStorage.set(this.appSettings.storageName.config, config);
  }

  public getConfiguration(): Settings {
    if (this.appSettings.storageName) {
      return this.localStorage.get(this.appSettings.storageName.config);
    } else {
      return null;
    }
  }

  public setAmbient(ambient: string) {
    this.localStorage.set(this.appSettings.storageName.ambientbase, ambient);
  }

  public getAmbient() {

    const ambient: any = new Object();
    const context = `${location.href.split('/')[3]}`;

    ambient.D_O = this.localStorage.get(`D_O_${context}`) ? 'D_O' : null;
    ambient.Q_O = this.localStorage.get(`Q_O_${context}`) ? 'Q_O' : null;
    ambient.P_O = this.localStorage.get(`P_O_${context}`) ? 'P_O' : null;
    ambient.D_S = this.localStorage.get(`D_S_${context}`) ? 'D_S' : null;
    ambient.Q_S = this.localStorage.get(`Q_S_${context}`) ? 'Q_S' : null;
    ambient.P_S = this.localStorage.get(`P_S_${context}`) ? 'P_S' : null;
    ambient.U_U = this.localStorage.get(`U_U_${context}`) ? 'U_U' : null;

    if (ambient.D_O) { ambient.cd = ambient.D_O; }
    else if (ambient.Q_O) { ambient.cd = ambient.Q_O; }
    else if (ambient.P_O) { ambient.cd = ambient.P_O; }
    else if (ambient.D_S) { ambient.cd = ambient.D_S; }
    else if (ambient.Q_S) { ambient.cd = ambient.Q_S; }
    else if (ambient.P_S) { ambient.cd = ambient.P_S; }
    else if (ambient.U_U) { ambient.cd = ambient.U_U; }
    else {
      if (this.appSettings.storageName) {
        ambient.cd = this.appSettings.storageName.ambient;
      } else {
        ambient.cd = null;
      }
    }

    return ambient.cd;
  }

  public checkStorage() {
    if (this.getAmbient()) {

      this.setNameStorage(this.getAmbient());

      this.appSettings.settings = this.getConfiguration();
      this.appSettings.isLoadConfig = true;

      document.getElementById('mainContent')
        .classList.add(this.appSettings.settings.theme.skin);

      this.setStatusInactivity(false);
      this.setStatusRequest(false);

    } else {

      this.appService.getConfiguration().subscribe((config: Settings) => {

        this.setNameStorage(config.application.ambiente);
        this.appSettings.settings = config;
        this.setAmbient(config.application.ambiente);
        this.setConfiguration(this.appSettings.settings);
        this.setStatusInactivity(false);
        this.setStatusRequest(false);
        this.appSettings.isLoadConfig = true;

        document.getElementById('mainContent')
          .classList.add(this.appSettings.settings.theme.skin);

      });

    }

  }

  public deleteStorage(stg: string): void {
    if (this.localStorage.get(stg)) {
      this.localStorage.remove(stg);
    }
  }

}
