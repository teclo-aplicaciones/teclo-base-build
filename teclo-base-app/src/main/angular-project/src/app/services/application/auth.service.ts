import { Injectable } from '@angular/core';
import { Environment } from '@environment/environment';
import { HttpClient } from '@angular/common/http';
import { WebstorageService } from '@services/application/webstorage.service';
import { AlertsService } from '@services/application/alerts.service';
import { Router } from '@angular/router';
import { AppSettings } from '@core/config/settings.config';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Settings } from '@models/app/application.model';
import { UserVO } from '@models/app/user.model';
import { InactivityVO } from '@models/app/application.model';
import { timer, Subscription, Subject } from 'rxjs';


const jwtHelper = new JwtHelperService();

@Injectable()
export class AuthService {

  private readonly API_URL = Environment.urlWs;
  private inactivity: InactivityVO;
  public Request = new Subject();

  constructor(
    private http: HttpClient,
    private wss: WebstorageService,
    private router: Router,
    private appSettings: AppSettings,
    readonly alert: AlertsService
  ) {
    this.inactivity = appSettings.inactivity;
  }

  public goLogin(user: object): any {
    return this.http.post<any>(`${this.API_URL}/login`, user);
  }

  public refreshToken(): void {
    this.http.get<any>(`${this.API_URL}/login/refresh`).subscribe(newToken => {
      this.wss.setToken(newToken.token);
      /*this.alert.toast(
        'Sesión actualizada', 'info', 'top', 2500
      );*/
      this.wss.setStatusInactivity(false);
    });
  }

  public goLogout(): any {

    this.wss.deleteStorage(this.appSettings.storageName.ambientbase);
    this.wss.deleteStorage(this.appSettings.storageName.token);
    this.wss.deleteStorage(this.appSettings.storageName.config);
    this.wss.setStatusInactivity(false);
    this.appSettings.isLoadConfig = false;
    this.appSettings.isLoadMenu = false;
    this.appSettings.isToken = false;

    delete this.appSettings.storageName;

    this.router.navigate(['/login']);

    delete this.appSettings.menu;
  }

  public isAuthenticated() {

    const token: string = this.wss.getToken();
    const config: Settings = this.wss.getConfiguration();

    if (!token) {
      if (!config || !config.tokenlenght) { return false; }
    }

    if (config.tokenlenght !== token.length) { return false; }

    if (jwtHelper.isTokenExpired(token)) { return false; } else { return true; }
  }

  public getDecodedToken(): UserVO {

    if (this.wss.getToken()) {

      const decoded = jwtHelper.decodeToken(this.wss.getToken());

      this.appSettings.user = new UserVO(
        decoded.userNum, decoded.sub, decoded.name,
        decoded.perfil, decoded.audience, decoded.created,
        decoded.exp, decoded.inactivity, decoded.pages,
        decoded.caja, decoded.deposito
      );

      return this.appSettings.user;

    } else { return; }

  }

  public getExpirationDate(): Date { return jwtHelper.getTokenExpirationDate(this.wss.getToken()); }

  public isTokenExpired(): boolean { return jwtHelper.isTokenExpired(this.wss.getToken()); }

  public getPayload(payload: string): any {

    const decodeToken = jwtHelper.decodeToken(this.wss.getToken());

    let payloadDecode = decodeToken[payload];

    if (!payloadDecode || payloadDecode === '') { return false; }

    if (payload === 'pages') { payloadDecode += ' /main /error /unauthorized /not-found /componentes /elementos '; }

    return payloadDecode;
  }

  public breakTimer(time: any) {
    if (time) { clearInterval(time); }
  }

  public goRequest(): void {
    this.Request.next(true);
    this.wss.setStatusRequest(true);
  }

}
