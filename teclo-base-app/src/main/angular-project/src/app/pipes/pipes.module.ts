// ANGULAR MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// PIPES
import { TruncateTextPipe } from '@pipes/truncate-text.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TruncateTextPipe
  ],
  exports: [
    TruncateTextPipe
  ]
})
export class PipesModule { }
