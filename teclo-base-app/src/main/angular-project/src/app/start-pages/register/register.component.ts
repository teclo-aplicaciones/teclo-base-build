import { Component, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Assets } from '@environment/environment';
import { AppValidators } from '@config/validators.config';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements AfterViewInit {

    @ViewChild('logoRegister',{static: false}) logoRegister: ElementRef;

    public router: Router;
    public form: FormGroup;
    public name: AbstractControl;
    public email: AbstractControl;
    public password: AbstractControl;
    public confirmPassword: AbstractControl;
    public assets = Assets;

    constructor(
        private fb: FormBuilder,
        private validator: AppValidators,
        router: Router
    ) {
        this.router = router;
        this.form = fb.group(
            {
                name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
                email: ['', Validators.compose([Validators.required, validator.email])],
                password: ['', Validators.required],
                confirmPassword: ['', Validators.required]
            },
            { validator: validator.matchingPasswords('password', 'confirmPassword') }
        );

        this.name = this.form.controls.name;
        this.email = this.form.controls.email;
        this.password = this.form.controls.password;
        this.confirmPassword = this.form.controls.confirmPassword;
    }

    public onSubmit(values: object): void {
        if (this.form.valid) { this.router.navigate(['/login']); }
    }

    ngAfterViewInit() {

        const img = this.logoRegister.nativeElement;

        setTimeout(() => { img.style.width = '76px'; }, 100);

        // document.getElementById('preloader').classList.add('hide');
    }
}
