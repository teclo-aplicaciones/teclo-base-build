import { Component, ViewEncapsulation, AfterViewInit, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppSettings } from '@core/config/settings.config';
import { AuthService, WebstorageService, AlertsService } from '@services/service.main';
import { Assets } from '@environment/environment';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, AfterViewInit {

    @ViewChild('logoLogin',{static: false}) logoLogin: ElementRef;
    @ViewChild('passwordEl',{static: false}) passwordEl: ElementRef;

    public nameAplication: string;
    public router: Router;
    public formLogin: FormGroup;
    // public email: AbstractControl;
    public username: AbstractControl;
    public password: AbstractControl;
    public msgError: string;
    public assets = Assets;
    public isViewPass = false;

    constructor(
        public appSettings: AppSettings,
        private authService: AuthService,
        private webstorageService: WebstorageService,
        private alert: AlertsService,
        router: Router,
        fb: FormBuilder
    ) {
        this.router = router;

        if ( !this.appSettings.isLoadConfig ) {
            webstorageService.checkStorage();
        }

        this.formLogin = fb.group({
            // 'email': ['', Validators.compose([Validators.required, this.validators.email])],
            username: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        });

        // this.email = this.formLogin.controls['email'];
        this.username = this.formLogin.controls.username;
        this.password = this.formLogin.controls.password;
    }

    ngOnInit(): void { this.nameAplication = this.appSettings.settings.name; }

    toggleViewPass() {

        this.isViewPass = !this.isViewPass;

        if (this.isViewPass) {
            this.passwordEl.nativeElement.type = 'text';
        } else {
            this.passwordEl.nativeElement.type = 'password';
        }

        this.passwordEl.nativeElement.focus();
    }

    goLogin(values: any): void {

        this.msgError = undefined;

        if (this.formLogin.valid) {

            const user = {
                username: values.username,
                password: values.password
            };

            if ( !this.webstorageService.getConfiguration() ) {

                this.alert.info(
                    'Se ha detectado la modificación del localStorage',
                    'Se recargará la aplicación para reestablecer los datos',
                    () => { window.location.reload(); }
                );

                return;
            }

            this.authService.goLogin(user).subscribe( (auth: any) => {

                this.appSettings.user = auth;
                this.appSettings.isToken = true;
                this.appSettings.settings.tokenlenght = auth.token.length;
                this.webstorageService.setToken(auth.token);
                this.webstorageService.setConfiguration(this.appSettings.settings);
                this.router.navigate(['app']);

            }, (badrequest: HttpErrorResponse) => {
                this.msgError = badrequest.error.message;

                if ( this.msgError ) {
                    this.passwordEl.nativeElement.focus();
                }
            });
        }
    }

    ngAfterViewInit() {

        const img = this.logoLogin.nativeElement;

        setTimeout( () => { img.style.width = '76px'; }, 100);

    }

}
