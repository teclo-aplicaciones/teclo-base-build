import { Component, OnInit } from '@angular/core';
import { AppSettings } from '@core/config/settings.config';
import { Settings } from '@core/models/app/application.model';
import { Assets } from '@environment/environment';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public settings: Settings;
  public assets = Assets;

  constructor( public appSettings: AppSettings) {
      this.settings = this.appSettings.settings;
   }

  ngOnInit() { }

}
