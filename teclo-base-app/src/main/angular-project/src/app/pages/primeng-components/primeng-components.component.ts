import { Component, OnInit } from '@angular/core';
import { ConfigurationService } from '@services/pages/service.pages';
import { AlertsService } from '@services/service.main';
import { SelectItem, MenuItem } from 'primeng/api';


interface City {
  name: string;
  code: string;
  isActive: boolean;
}

@Component({
  selector: 'app-primeng-components',
  templateUrl: './primeng-components.component.html'
})
export class PrimengComponentsComponent implements OnInit {

  public cities1: SelectItem[];
  public cities2: City[];
  public targetList: [] = [];
  public selectedRadio: string;
  public displayDialog: boolean;
  public cols: object[];
  public selectedColumns: any[];
  public peoples: object[];
  public images: object[];
  public items: MenuItem[];
  public activeIndex = 1;
  public types: SelectItem[];

  constructor(
    protected configurationService: ConfigurationService,
    readonly alert: AlertsService
  ) {
    this.cities1 = [
      { label: 'Select City', value: { id: null, isActive: false } },
      { label: 'New York', value: { id: 1, name: 'New York', isActive: true, code: 'NY' } },
      { label: 'Rome', value: { id: 2, name: 'Rome', isActive: false, code: 'RM' } },
      { label: 'London', value: { id: 3, name: 'London', isActive: false, code: 'LDN' } },
      { label: 'Istanbul', value: { id: 4, name: 'Istanbul', isActive: false, code: 'IST' } },
      { label: 'Paris', value: { id: 5, name: 'Paris', isActive: false, code: 'PRS' } }
    ];

    // An array of cities
    this.cities2 = [
      { name: 'New York', code: 'NY', isActive: false },
      { name: 'Rome', code: 'RM', isActive: false },
      { name: 'London', code: 'LDN', isActive: true },
      { name: 'Istanbul', code: 'IST', isActive: false },
      { name: 'Paris', code: 'PRS', isActive: false }
    ];

    this.types = [
      {label: 'Paypal', value: 'PayPal', icon: 'fa fa-fw fa-cc-paypal'},
      {label: 'Visa', value: 'Visa', icon: 'fa fa-fw fa-cc-visa'},
      {label: 'MasterCard', value: 'MasterCard', icon: 'fa fa-fw fa-cc-mastercard'}
    ]

    this.cols = [
      { field: 'id', header: 'ID' },
      { field: 'name', header: 'NOMBRE' },
      { field: 'age', header: 'EDAD' },
      { field: 'nacionality', header: 'NACIONALIDAD' },
      { field: 'date', header: 'FECHA CREACIÓN' },
      { field: 'gender', header: 'GÉNERO' }
    ];
    this.selectedColumns = this.cols;
    this.peoples = [
      { id: 1, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 2, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 3, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 4, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 5, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 6, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 7, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 8, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 9, name: 'Margarita', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 10, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 11, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 12, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 13, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 14, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 15, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 16, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 17, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 18, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 19, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 20, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' },
      { id: 21, name: 'Carolina', age: 26, nacionality: 'Mexicana', date: '30/05/2019 14:10', gender: 'Femenino' }
    ];

    this.selectedRadio = this.cities2[2].code;

    this.images = [
      {
        source: 'https://www.sciencemag.org/sites/default/files/styles/article_main_large/public/sn-dark%20web.png?itok=O7J9fokh',
        thumbnail: 'https://www.sciencemag.org/sites/default/files/styles/article_main_large/public/sn-dark%20web.png?itok=O7J9fokh',
        title: 'Web uno'
      },
      {
        source: 'https://1.bp.blogspot.com/-qEnMk64JS24/VqyoIgdkWGI/AAAAAAAAELk/4zMqL-kbClg/s1600/best-web-browser.jpg',
        thumbnail: 'https://1.bp.blogspot.com/-qEnMk64JS24/VqyoIgdkWGI/AAAAAAAAELk/4zMqL-kbClg/s1600/best-web-browser.jpg',
        title: 'Web dos'
      },
      {
        source: 'https://as01.epimg.net/meristation/imagenes/2019/03/12/betech/1552388984_411255_1552389117_noticia_normal.jpg',
        thumbnail: 'https://as01.epimg.net/meristation/imagenes/2019/03/12/betech/1552388984_411255_1552389117_noticia_normal.jpg',
        title: 'Web tres'
      },
      {
        source: 'https://blog.benzahosting.cl/wp-content/uploads/2016/01/rl8vzy74mz9drpztjyyw.png',
        thumbnail: 'https://blog.benzahosting.cl/wp-content/uploads/2016/01/rl8vzy74mz9drpztjyyw.png',
        title: 'Web cuatro'
      }
    ];

    this.items = [
      { label: 'Step 1', command: (event: any) => this.activeIndex = 0, icon: 'pi pi-pw pi-file'},
      { label: 'Step 2', command: (event: any) => this.activeIndex = 1, icon: 'pi pi-pw pi-file',
        items: [
          {label: 'Delete', icon: 'pi pi-fw pi-trash'},
          {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
        ]
      },
      { label: 'Step 3', command: (event: any) => this.activeIndex = 2, icon: 'pi pi-pw pi-file',
        items: [
            {label: 'Delete', icon: 'pi pi-fw pi-trash'},
            {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
        ]
      },
      { label: 'Step 4', command: (event: any) => this.activeIndex = 3, icon: 'pi pi-pw pi-file',
        items: [
            {label: 'Delete', icon: 'pi pi-fw pi-trash'},
            {label: 'Refresh', icon: 'pi pi-fw pi-refresh'}
        ]
      }
    ];
  }

  ngOnInit() { }

  public testToggleModal(event: string) {
    if (event === 'show') {
      this.alert.toast('Se abrió el modal', 'success', 'top');
    } else {
      this.alert.toast('Se cerró el modal', 'warning', 'top');
    }
  }

}
