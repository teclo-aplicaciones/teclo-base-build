import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { PagesComponent } from '@pages/pages.component';
import { HomeComponent } from '@core/pages/home/home.component';
import { InternalServerComponent } from '@shared/errors/internal-server/internal-server.component';
import { NotFoundComponent } from '@shared/errors/not-found/not-found.component';
import { UnauthorizedComponent } from '@shared/errors/unauthorized/unauthorized.component';

import { PageGuard } from '@core/services/service.main';
import { PrimengComponentsComponent } from '@pages/primeng-components/primeng-components.component';
import { ElementosBaseComponent } from './elementosBase/elementosBase.component';
import { ChangePasswordComponent } from './administration/change-password/change-password.component';

export const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        canActivateChild : [PageGuard],
        children: [
            { path: '', redirectTo: 'main', pathMatch: 'full' },
            { path: 'main', component: HomeComponent, data: { breadcrumb: 'Inicio' } },
            {
                // CARGA MÓDULO DE ADMINISTRACIÓN
                path: 'administracion',
                loadChildren: () => import('app/pages/administration/administration.module').then(m => m.AdministrationModule),
                data: { breadcrumb: 'Administración' },
                children: [
                  { path: 'cambiarClave', component: ChangePasswordComponent },
                  { path: 'configapplicacion', component: ChangePasswordComponent }
               ],
            },
            { path: 'error', component: InternalServerComponent, data: { breadcrumb: 'Proceso inesperado' } },
            { path: 'unauthorized', component: UnauthorizedComponent, data: { breadcrumb: 'Acceso no autorizado' } },
            { path: 'componentes', component: PrimengComponentsComponent, data: { breadcrumb: 'Componentes primeng' } },
            { path: 'elementos', component: ElementosBaseComponent, data: { breadcrumb: 'Elementos guía primeng' } },



            { path: '**', component: NotFoundComponent, data: { breadcrumb: 'Página no encontrada' } }
        ]
    }
];

export const RoutingPages: ModuleWithProviders = RouterModule.forChild(routes);
