import { Component, OnInit } from '@angular/core';
import { MenuItem, SelectItem } from 'primeng';
import {ToggleButtonModule} from 'primeng/togglebutton';
import { AlertsService } from '@core/services/application/alerts.service';

@Component({
  selector: 'app-boton',
  templateUrl: './boton.component.html',
  styleUrls: ['./boton.component.scss']
})
export class BotonComponent implements OnInit {

  items: MenuItem[];
  types: SelectItem[];
  selectedType: string;
  selectedTypes: string[] = ['PayPal','MasterCard'];
  checked1:boolean  = false;
  checked2:boolean = true;

  constructor(private messageService:AlertsService) { 
    
  }

  ngOnInit() {
    this.items = [
      {label: 'Update', icon: 'pi pi-refresh', command: () => {
          this.update();
      }},
      {label: 'Delete', icon: 'pi pi-times', command: () => {
          this.delete();
      }},
      {label: 'Angular.io', icon: 'pi pi-info', url: 'http://angular.io'},
      {separator: true},
      {label: 'Setup', icon: 'pi pi-cog', routerLink: ['/setup']}
    ];

    this.types = [
      {label: 'Paypal', value: 'PayPal', icon: 'fa fa-fw fa-cc-paypal'},
      {label: 'Visa', value: 'Visa', icon: 'fa fa-fw fa-cc-visa'},
      {label: 'MasterCard', value: 'MasterCard', icon: 'fa fa-fw fa-cc-mastercard'}
    ];

  }

  save(severity: string) {
    this.messageService.viewAlert('Button Clicked', 'save', severity);
  }
  update() {
    this.messageService.viewAlert('Button Clicked', 'update', 'top');
  }
  delete() {
    this.messageService.viewAlert('Button Clicked', 'delete', 'top');
  }

}
