import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var NgForm:any;

@Component({
  selector: 'app-formreactivo',
  templateUrl: './formreactivo.component.html'
})
export class FormReactiveComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  
  constructor(private formBuilder: FormBuilder) { }
  title = 'Form Reactive en Angular 8';

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required,Validators.email]],
      phone: ['', Validators.required,Validators.minLength(8)],
      password: ['', [Validators.required,Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      tnc: ['', Validators.required]
    });

  }

  get fval() { 
    return this.registerForm.controls; 
  }

  signup(){
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    alert('Campos validados correctamente');  
  }
}
