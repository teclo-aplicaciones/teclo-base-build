import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicTablesComponent } from './basic-tables/basic-tables.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

export const routes = [
  { path: '', redirectTo: 'basic-tables', pathMatch: 'full'},
  { path: 'basic-tables', component: BasicTablesComponent, data: { breadcrumb: 'Basic Tables' } },
  { path: 'dynamic-tables', loadChildren: 'app/pages/tables/dynamic-tables/dynamic-tables.module#DynamicTablesModule', data: { breadcrumb: 'Dynamic Tables' } }
];

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
   ],
  declarations: [
    BasicTablesComponent
  ],
  exports:[
    BasicTablesComponent
  ]
})
export class TablesModule { }
