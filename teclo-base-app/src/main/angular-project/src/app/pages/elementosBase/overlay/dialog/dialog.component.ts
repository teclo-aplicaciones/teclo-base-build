import { Component } from '@angular/core';

@Component({
    selector:'app-dialog',
    templateUrl: './dialog.component.html'
})

export class DialogComponent {
    display: boolean=false;

    showDialog() {
        this.display = true;
    }

    hideDialog() {
        this.display = false;
    }

}