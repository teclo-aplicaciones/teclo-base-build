import {Component} from '@angular/core';
import {Car} from '../model/car';
import { CarService } from './carservice';
import { OverlayPanel } from 'primeng/overlaypanel/overlaypanel';

@Component({
    selector: 'app-overlaypanel',
    templateUrl: './overlaypanel.component.html',
    providers: [CarService]
})
export class OverlayPanelComponent {

    cars1: Car[];
    cars2: Car[];
    selectedCar: Car;
    
    constructor(private carService: CarService) { }

    ngOnInit() {
        this.carService.getCarsSmall().then(cars => this.cars1 = cars);
        this.carService.getCarsSmall().then(cars => this.cars2 = cars);
    }
    
    selectCar(event,car: Car, overlaypanel: OverlayPanel) {
        this.selectedCar = car;
        overlaypanel.toggle(event);
    }
}