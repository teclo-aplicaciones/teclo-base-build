import { Component } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent {
    disabled: boolean= true;
    blockSpecial: RegExp = /^[^<>*!]+$/
    blockSpace: RegExp = /[^\s]/; 
    ccRegex: RegExp = /[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}$/; 
    cc: string; 

    constructor() {
           
    }

}
