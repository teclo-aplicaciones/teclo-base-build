import {Component} from '@angular/core';

@Component({
    selector : 'app-lightbox',
    templateUrl: './lightbox.component.html'
})

export class LightboxComponent {

    images: any[];

    constructor() {
        this.images = [];
        this.images.push({source:'assets/img/lightbox/angular.png', thumbnail: 'assets/img/lightbox/angular.png', title:'Angular'});
        this.images.push({source:'assets/img/lightbox/saas.png', thumbnail: 'assets/img/lightbox/saas.png', title:'Saas'});
        this.images.push({source:'assets/img/lightbox/ts.png', thumbnail: 'assets/img/lightbox/ts.png', title:'Ts'});
   }
}
