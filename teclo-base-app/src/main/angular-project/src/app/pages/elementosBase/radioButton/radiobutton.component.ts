import {Component} from '@angular/core';

@Component({
    selector: 'app-radiobutton',
    templateUrl: './radiobutton.component.html'
})
export class RadioButtonComponent {

    val1: string;
    val2: string = 'Option 2';
    selectedCities: string[] = [];
    selectedCategories: string[] = ['Technology', 'Sports'];
    checked: boolean = false;

}