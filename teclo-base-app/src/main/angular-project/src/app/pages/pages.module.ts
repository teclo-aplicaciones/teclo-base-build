// ANGULAR MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// APP MODULES
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AppTranslateModule } from '@components/app-translate.module';
import { PipesModule } from '@pipes/pipes.module';
import { ServicePagesModule } from '@services/pages/service-pages.module';
import {SplitButtonModule} from 'primeng/splitbutton';

// COMPONENTS
import { HeaderComponent } from '@shared/header/header.component';
import { UserMenuComponent } from '@shared/user-menu/user-menu.component';
import { SidebarComponent } from '@shared/sidebar/sidebar.component';
import { VerticalMenuComponent } from '@shared/menu/vertical-menu/vertical-menu.component';
import { HorizontalMenuComponent } from '@shared/menu/horizontal-menu/horizontal-menu.component';
import { BreadcrumbComponent } from '@shared/breadcrumb/breadcrumb.component';
import { FooterComponent } from '@shared/footer/footer.component';
import { BackTopComponent } from '@shared/back-top/back-top.component';
import { PagesComponent } from '@pages/pages.component';

import { HomeComponent } from '@pages/home/home.component';
import { ComponentsModule } from '@components/components.module';
import { PrimengComponentsComponent } from '@pages/primeng-components/primeng-components.component';
import { ElementosBaseComponent } from '@pages/elementosBase/elementosBase.component';


// ROUTES
import { RoutingPages } from '@pages/pages.routing';
// INITIALIZATION CONFIG
import { DirectivesPrimeNg } from '@directives/directives-primeng';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import {ToolbarModule} from 'primeng/toolbar';
import { SharedModule } from '../shared/shared.module';
import { GridComponent } from './elementosBase/grid/grid.component';
import { BotonComponent } from './elementosBase/botones/boton.component';
import { DropdownComponent } from './elementosBase/dropdown/dropdown.component';
import {ToggleButtonModule} from 'primeng/togglebutton';
import { InputComponent } from './elementosBase/input/input.component';
import {KeyFilterModule} from 'primeng/keyfilter';
import { MensajeComponent } from './elementosBase/messages/mensajes/mensajes.component';
import { ToastComponent } from './elementosBase/messages/toast/toast.component';
import { FileUploadComponent } from './elementosBase/fileupload/fileupload.component';
import { ConfirmDialogComponent } from './elementosBase/overlay/confirmDialog/confirmDialog.component';
import { DialogComponent } from './elementosBase/overlay/dialog/dialog.component';
import { CalendarioComponent } from './elementosBase/calendar/calendaro.component';
import { TablesModule } from './elementosBase/tables/tables.module';
import { DynamicTablesModule } from './elementosBase/tables/dynamic-tables/dynamic-tables.module';
import { DirectivesModule } from '@core/directives/directives.module';
import { LightboxComponent } from './elementosBase/overlay/lightbox/lightbox.component';
import { AccordionComponent } from './elementosBase/panel/accordion/accordion.component';
import { OverlayPanelComponent } from './elementosBase/overlay/overlaypanel/overlaypanel.component';
import { RadioButtonComponent } from './elementosBase/radioButton/radiobutton.component';
import { AdministrationModule } from './administration/administration.module';
import { DragDropComponent } from './elementosBase/draganddrop/draganddrop.component';
import { FormReactiveComponent } from './elementosBase/formulario/formreactivo/formreactivo.component';
import { FormTemplateComponent } from './elementosBase/formulario/formtemplate/formtemplate.component';
import { ConfirmEqualValidatorDirective } from './elementosBase/formulario/formtemplate/confirm-equal-validator.directive';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    RoutingPages,
    ServicePagesModule,
    PipesModule,
    ComponentsModule,
    AppTranslateModule,
    DirectivesPrimeNg,
    ToolbarModule,
    SplitButtonModule,
    SharedModule,
    AdministrationModule,
    ToggleButtonModule,
    KeyFilterModule,
    TablesModule,
    DynamicTablesModule,
    DirectivesModule
  ],
  declarations: [
    GridComponent,
    PrimengComponentsComponent,
    ElementosBaseComponent,
    BotonComponent,
    DropdownComponent,
    ToastComponent,
    MensajeComponent,
    FileUploadComponent,
    ConfirmDialogComponent,
    DialogComponent,
    InputComponent,
    CalendarioComponent,
    LightboxComponent,
    AccordionComponent,
    OverlayPanelComponent,
    DragDropComponent,
    FormReactiveComponent,
    FormTemplateComponent,ConfirmEqualValidatorDirective,
    RadioButtonComponent
   ],
  providers: [

  ],
  exports:[
    GridComponent,
    BotonComponent,
    DropdownComponent,
    ToastComponent,
    MensajeComponent,
    FileUploadComponent,
    ConfirmDialogComponent,
    DialogComponent,
    ToggleButtonModule,
    InputComponent,
    KeyFilterModule,
    CalendarioComponent,
    LightboxComponent,
    AccordionComponent,
    OverlayPanelComponent,
    DragDropComponent,
    FormReactiveComponent,
    FormTemplateComponent,
    RadioButtonComponent
  ]
})
export class PagesModule { }
