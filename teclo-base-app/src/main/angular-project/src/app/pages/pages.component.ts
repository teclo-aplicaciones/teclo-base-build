import { Component, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { AppSettings } from '@core/config/settings.config';
import { Settings, ThemeVO } from '@models/app/application.model';
import { WebstorageService, AppService } from '@services/service.main';
import { TranslateService } from '@ngx-translate/core';


@Component({
    selector: 'app-pages',
    templateUrl: './pages.component.html',
    styleUrls: ['./pages.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PagesComponent implements OnInit {

    public showMenu = false;
    public showSetting = false;
    public menuOption: string;
    public menuTypeOption: string;
    public settings: Settings;
    public menus = [
        {id: 1, code: 'vertical', name: 'Vertical', nameEn: 'Vertical'},
        {id: 2, code: 'horizontal', name: 'Horizontal', nameEn: 'Horizontal'}
    ];
    public menuTypes = [
        {id: 1, code: 'default', name: 'Por defecto', nameEn: 'Default'},
        {id: 2, code: 'compact', name: 'Compacto', nameEn: 'Compact'},
        {id: 3, code: 'mini', name: 'Solo íconos', nameEn: 'Only icons'}
    ];
    public themes: ThemeVO[] = [
        new ThemeVO(1, 'Teclo light', 'tema-tecloLight', 1, true, true, '#ecf0f1', '#ecf0f1'),
        new ThemeVO(2, 'Ligero', 'light', 1, false, false, '#ececec', '#ececec'),
        new ThemeVO(3, 'Oscuro', 'dark', 1, false, false, '#262626', '#262626'),
        new ThemeVO(4, 'Azul', 'blue', 1, false, false, '#1875D1', '#1875D1'),
        new ThemeVO(5, 'Verde', 'green', 1, false, false, '#00786A', '#00786A'),
        new ThemeVO(6, 'Combinado', 'combined', 1, false, true, '#262626', '#f5f5f5'),
        new ThemeVO(7, 'Morado', 'purple', 1, false, false, '#7A1EA1', '#7A1EA1'),
        new ThemeVO(8, 'Naranja', 'orange', 1, false, false, '#F47B00', '#F47B00'),
        new ThemeVO(9, 'Café', 'brown', 1, false, false,  '#5C3F36', '#5C3F36'),
        new ThemeVO(10, 'Gris', 'grey', 1, false, false, '#445963', '#445963'),
        new ThemeVO(11, 'Rosado', 'pink', 1, false, false, '#C1175A', '#C1175A')
    ];

    constructor(
        public appSettings: AppSettings,
        public translate: TranslateService,
        private readonly wss: WebstorageService,
        private readonly appService: AppService
    ) {
        this.settings = this.appSettings.settings;
    }

    ngOnInit() {
        if (window.innerWidth <= 768) {
            this.settings.theme.showMenu = false;
            this.settings.theme.sideChatIsHoverable = false;
        }
        this.showMenu = this.settings.theme.showMenu;
        this.menuOption = this.settings.theme.menu;
        this.menuTypeOption = this.settings.theme.menuType;

        this.appService.getThemes().subscribe( (themes: ThemeVO) => {
            console.log(this.appSettings.configurationApp.tema);
        });
    }

    public chooseMenu( menu: string ) {
        this.settings.theme.menu = menu;
    }

    public chooseMenuType( menuType: string ) {

        this.settings.theme.menuType = menuType;

        if (menuType === 'mini') {
            jQuery('.menu-item-link').tooltip('enable');
        } else {
            jQuery('.menu-item-link').tooltip('disable');
        }
    }

    public changeTheme( theme: ThemeVO ) {

        document.getElementById('mainContent')
          .classList.remove(this.settings.theme.skin);

        this.settings.theme.skin = theme.cdTema;
        this.wss.setConfiguration(this.settings);

        document.getElementById('mainContent')
          .classList.add(this.settings.theme.skin);

        this.themes.map( item => item.isActive = false );
        theme.isActive = true;
    }

    @HostListener('window:resize')
    public onWindowResize(): void {

        const showMenu = !this._showMenu();

        if (this.showMenu !== showMenu) {
            this.showMenuStateChange(showMenu);
        }
        this.showMenu = showMenu;
    }

    public showMenuStateChange(showMenu: boolean): void {
        this.settings.theme.showMenu = showMenu;
    }

    private _showMenu(): boolean {
        return window.innerWidth <= 768;
    }

}
