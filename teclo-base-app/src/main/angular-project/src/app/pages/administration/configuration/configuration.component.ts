import { Component, OnInit } from '@angular/core';
import { ConfigurationService } from '@services/pages/service.pages';
import { AlertsService } from '@services/service.main';
import { SelectItem, MenuItem } from 'primeng/api';


interface City {
  name: string;
  code: string;
  isActive: boolean;
}

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  constructor(
    protected configurationService: ConfigurationService,
    readonly alert: AlertsService
  ) { }

  ngOnInit() { }

}
