// ANGULAR MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppTranslateModule } from '@core/components/app-translate.module';
// COMPONENTS
import { ConfigurationComponent } from './configuration/configuration.component';
import { UsersComponent } from './users/users.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
// INITIALIZATION CONFIG
import { DirectivesPrimeNg } from '@directives/directives-primeng';

export const routes = [
  { path: '', pathMatch: 'full' },
  { path: 'configuracion', component: ConfigurationComponent, data: { breadcrumb: 'Configuración' } },
  { path: 'usuarios', component: UsersComponent, data: { breadcrumb: 'Usuarios' } },
  { path: 'perfiles', component: ProfilesComponent, data: { breadcrumb: 'Perfiles' } },
  { path: 'cambioClave', component: ChangePasswordComponent, data: { breadcrumb: 'Cambiar contraseña' } }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    AppTranslateModule,
    DirectivesPrimeNg
  ],
  declarations: [
    ConfigurationComponent,
    UsersComponent,
    ProfilesComponent,
    ChangePasswordComponent
  ],
  exports: [
    ConfigurationComponent,
    UsersComponent,
    ProfilesComponent,
    ChangePasswordComponent
  ]
})

export class AdministrationModule { }
