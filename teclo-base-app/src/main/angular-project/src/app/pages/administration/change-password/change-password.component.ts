import { Component, OnInit } from '@angular/core';
import { CambioContrasenia } from '@models/administracion/cambio-contrasenia.model';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  public cambioContrasenia:CambioContrasenia;
  public formCambioContrasenia: FormGroup;

  constructor() {
    this.cambioContrasenia = new CambioContrasenia(null,null,null);
  }

  ngOnInit() {
  }


  public cambiarContrasenia(values: any){


    if (this.formCambioContrasenia.valid) {

      const user = {
          username: values.username,
          password: values.password
      };

    console.log(this.cambioContrasenia);
    }
  }
}

