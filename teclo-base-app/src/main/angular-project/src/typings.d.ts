/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}
declare var jQuery: any;
interface Document {
    msExitFullscreen: () => void;
    mozCancelFullScreen: () => void;
    mozFullScreenElement: () => void;
    msFullscreenElement: () => void;
    webkitExitFullscreen: () => void;
    webkitFullscreenElement: () => void;
    //fullscreenElement: () => void;
}
declare var L: any;  // leaflet
