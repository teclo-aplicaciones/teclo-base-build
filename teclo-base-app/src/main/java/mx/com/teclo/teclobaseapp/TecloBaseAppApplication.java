package mx.com.teclo.teclobaseapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TecloBaseAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(TecloBaseAppApplication.class, args);
	}

}

